TehSFD95 Official Website
=========================

This is git repository for ***Theran Software Freedom Day of Year 1395***.

Licensing:
----------

Codes are licenced under GNU GPLv3+ and other contents are under
CC-BY-SA 4.0 International.  
Please refer to LICENSE file for more details.

Contribute:
------------

Contributors just follow google's advices and codig style suggestion of your
language.
